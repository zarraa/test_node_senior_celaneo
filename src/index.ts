import cors from 'cors';
import express from 'express';
import {API_PORT, MONGO_URI} from "./utils/constant";
import {ExceptionsHandler} from "./middlewares/exceptions.handler";
import {UnknownRoutesHandler} from "./middlewares/unknownRoutes.handler";
import {PollutionRouter} from "./resources/pollution/pollution.router";
import mongoose from "mongoose";
//require('./utils/cron')

/**
 * Creation  "application" express
 */
const app=express()

/**
 * Parse request body into JSON
 */
app.use(express.json())

/**
 *  allow all domain names to make requests on our API.
 */
app.use(cors())

/**
 *  connexion BDD
 */
mongoose.connect(MONGO_URI)

/**
* Homepage
*/
app.get('/', (req, res) => res.send('AIR QUALITY'));

app.use('/iqair',PollutionRouter)

/**
 * For all other undefined routes, an error is returned
 */
app.all('*', UnknownRoutesHandler)

/**
 * Error management
 */
app.use(ExceptionsHandler)

app.listen(API_PORT)

export {app}
