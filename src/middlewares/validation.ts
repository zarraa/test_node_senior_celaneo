import {NextFunction, Request, RequestHandler, Response} from "express";
import {check, validationResult} from "express-validator";

export const ValidationMiddleware = async (req: Request, res: Response, next: NextFunction) => {
    await check("lat", "latitude cannot be blank").not().isEmpty().run(req);
    await check("lon", "longitude cannot be blank").not().isEmpty().run(req);

    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400)
            .json({errors: errors.array()})
    }
    return next();

}


