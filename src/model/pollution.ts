import mongoose from "mongoose";

const pollutionSchema = new mongoose.Schema({
    city:String,
    ts: Date,
    aqius: Number,
    mainus: String,
    aqicn: Number,
    maincn: String
}, {timestamps: true})


const Pollution =mongoose.model('pollution',pollutionSchema)

export { Pollution}
