
import {Router,Request, Response} from "express";
import {PollutionService} from "./pollution.service";
import {BadRequestException} from "../../utils/exceptions";




/**
 * Creation`Router` Express`
 */
const PollutionRouter = Router()

/**
 * Instance of PollutionService
 */
const service = new PollutionService()

/**
 *  Get air quality by  longitude and latitude
 *
 * @param {Request} req
 * @param {Response} res
 * @return {*}
 */
export const getPollution= async (req: Request, res: Response) => {
    try {
        const {lat, lon} = req.body
        const response = await service.getPollution(lat, lon)
        if (response['data']['status']) {
            return res
                .status(200)
                .json({result: {pollution: response['data']['data']['current']['pollution']}})
        } else
            throw new BadRequestException('error in API iqair')
    } catch (err) {

        throw new BadRequestException('error')

    }

}

/**
 *  Get DATETIME where the city  Paris is the most polluted
 *
 * @param {Request} req
 * @param {Response} res
 * @return {*}
 */
export const getMaxPollution= async (req: Request, res: Response) => {
    const response:any = await service.getMaxPollution()
    return res.status(200).json({result: response.length ? response[0]['date'] : null})
}




