import {Router} from "express";
 import * as pollutionCtrl from "./pollution.controller"
import {ValidationMiddleware} from "../../middlewares/validation";

const PollutionRouter = Router()

PollutionRouter.post('/pollution', ValidationMiddleware,pollutionCtrl.getPollution)
PollutionRouter.get('/pollution/max',pollutionCtrl.getMaxPollution)


export {PollutionRouter}
