import {IQAIR_kEY, IQAIR_URI} from "../../utils/constant";
import axios from "axios";
import {Pollution} from "../../model/pollution";
import {Ipollution} from "../../types/pollution.type";


export class PollutionService {
    /**
     * get  pollution of api 'http://api.airvisual.com/v2/nearest_city'
     * @param {number} lat
     * @param {number} lon
     */
    async getPollution(lat: any, lon: any) {
        const uri = `${IQAIR_URI}?lat=${lat}&lon=${lon}&key=${IQAIR_kEY}`
        return await axios.get(uri);
    }


    /**
     * get max pollution in paris
     */
    async getMaxPollution() {
        const pop=[
            {$match: {city: 'paris'}},
            {
                $group: {
                    _id: '$city',
                    aqius: {$max: "$aqius"},
                    date: {$addToSet: '$ts'}
                }
            },
            {
                $project: {
                    city: '$city',
                    aqius: 1,
                    date: {$first: '$date'}

                }
            }
        ]
        return await Pollution.aggregate(pop)
    }


    /**
     * Add population by city 'paris'
     *
     *  @param {number} lat
     *  @param {number} lon
     *  @param {string} country
     *  @param {string} city
     */
    async create(lat:number,lon:number,country:string,city:string) {

        const uri = `${IQAIR_URI}?lat=${lat}&lon=${lon}&country=${country}&city=${city}&key=${IQAIR_kEY}`
        const response = await axios.get(uri);
        if (response['data']['status']) {
            const dataPollution=response['data']['data']['current']['pollution']
            dataPollution.city=city;
            const newPollution: Ipollution = new Pollution(dataPollution)
            return await Pollution.create(newPollution)
        }
    }

}
