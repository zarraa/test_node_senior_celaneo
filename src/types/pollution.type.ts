export interface Ipollution {
    city:string,
    ts: Date,
    aqius: number,
    mainus: string,
    aqicn: number,
    maincn: string
}
