import environment from "../../environnments/environment";

let _mongo_uri = `mongodb://${environment().database.user}:${environment().database.pwd}@${environment().database.hosts}/${
    environment().database.database
}?authSource=${environment().database.authSource}`;

export const MONGO_URI = _mongo_uri
export const API_PORT = environment().port
export const IQAIR_kEY = environment().iqair_api_key
export const IQAIR_URI = environment().iqair_url
export const LAT = 48.856613
export const LON =  2.352222
export const COUNTRY =  "France"
export const CITY =  "paris"

