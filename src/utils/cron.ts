import {PollutionService} from "../resources/pollution/pollution.service";
import {CITY, COUNTRY, LAT, LON} from "./constant";

var cronJob = require('cron').CronJob;
const service = new PollutionService()
/*
  run cron every minute and save object pollution of city in BDD

 */
var myJob = new cronJob('59 * * * * *', function(){

    console.log("cron ran")
    service.create(LAT,LON,COUNTRY,CITY)
});

myJob.start();
