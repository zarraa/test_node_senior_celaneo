import request from "supertest";
import {app} from "../src";

//const app = require('../src/index')

describe('GET /iqair/pollution/max', () => {
    it("should return 200 OK", (done) => {
        request(app).get("/iqair/pollution/max")
            .expect(200, done);
    });
})


describe('POST /iqair/pollution', () => {

        it('should get pollution ', async () => {
            const res = await request(app)
                .post('/iqair/pollution')
                .send({"lat":35.98,"lon":140.33})
            expect(res.statusCode).toEqual(200)

        })

})
